import { TestBed } from '@angular/core/testing';

import { CalculadoraService } from './calculadora.service';

describe('A classe Calculadora', () => {
  let calculadora: CalculadoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    calculadora = TestBed.inject(CalculadoraService);
  });

  //funcionalidade
  describe('deve realizar cálculos', () => {

    //caso de teste
    it('de divisão entre números inteiros', () => {
      const result = calculadora.Dividir(8, 4);
      expect(result).toBe(2);
    });

    //caso de teste
    it('de divisão com exceção do zero', () => {
      const result = calculadora.Dividir(8, 0);
      expect(typeof result).toEqual('string');
    });

    it('de multiplicação entre números', () => {
      const result = calculadora.Multiplicar(5, 5);
      expect(result).toBe(25);
    });

    it('de soma entre números', () => {
      const result = calculadora.Somar(5, 5);
      expect(result).toBe(10);
    });

    it('de subtração entre números', () => {
      const result = calculadora.Subtrair(7, 5);
      expect(result).toBe(2);
    });

  });

});
