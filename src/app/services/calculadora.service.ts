import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {
  private value: string = '';
  constructor() { }

  Dividir(dividendo, divisor): number | string{
    if(divisor == 0){
      return 'Não é possível dividir por zero.';
    }
    const res = Number(dividendo) / Number(divisor);
    return res;
  }

  Multiplicar(multiplicando, multiplicador): number | string{
    const res = multiplicando * multiplicador;
    return res;
  }

  Somar(digito1, digito2): number{
    const res = Number(digito1) + Number(digito2);
    return res;
  }
  Subtrair(digito1, digito2): number | string{
    const res = Number(digito1) - Number(digito2);
    return res;
  }

  btnCalc(value){
    this.value = value;
    return this.value;
  }
  
  Calcular(res){
    try
    {
      return eval(res);
    }
    catch(e)
    {
      e.message = "Formato de cálculo inválido";
      return e.message;
    }
  }
}
