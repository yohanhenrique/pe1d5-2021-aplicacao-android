import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ContaService {
  colletion: AngularFirestoreCollection;
  collection: any;

  constructor(
    private db: AngularFirestore
  ) { }

  registraConta(conta){
    conta.id = this.db.createId();
    this.colletion = this.db.collection("conta");
    return this.colletion.doc(conta.id).set(conta);
  }

  lista(tipo){
    this.colletion = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.colletion.valueChanges();
  }

  remove(conta){
    this.colletion = this.db.collection('conta');
    this.colletion.doc(conta.id).delete();

  }

  edita(conta){
    this.colletion = this.db.collection('conta');
    this.colletion.doc(conta.id).update(conta);
  }

  /**
   * Totaliza as contas de acordo com seu tipo
   * @param tipo: string - modalidade de contas
   */
  total(tipo, mes){

    this.collection = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.collection.get((snap => {
      let cont = 0;
      let sum = 0;

      snap.docs.map(doc => { 
            const conta = doc.data()
            const valor = parseFloat( conta.valor);
            sum += valor;
            cont++;
      });

      return { num: cont, valor: sum};
    }));
  }
}
