import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalculadoraService } from '../services/calculadora.service';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.scss'],
})
export class CalculadoraComponent implements OnInit {
  titulo = 'Calculadora';
  resultado: number | string = '';

  digito1: number | string = '';
  digito2: number | string = '';

  res: string = '';
  inputResultado: string = '';

  
  constructor(
    private calculadora: CalculadoraService
  ) { }

  ngOnInit(): void { }

  Dividir(){
    console.log(this.digito1);
    this.resultado = this.calculadora.Dividir(this.digito1, this.digito2);
  }
  Multiplicar(){
    this.resultado = this.calculadora.Multiplicar(this.digito1, this.digito2);
  }
  Somar(){
    this.resultado = this.calculadora.Somar(this.digito1, this.digito2);
  }
  Subtrair(){
    this.resultado = this.calculadora.Subtrair(this.digito1, this.digito2);
  }

  btnCalc(btnValue){
    this.res += this.calculadora.btnCalc(btnValue);
    console.log(this.res);
  }

  Calcular(){
    this.inputResultado = this.calculadora.Calcular(this.res);
    console.log(this.res);
    this.res = '';
  }

}
