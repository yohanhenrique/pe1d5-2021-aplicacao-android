// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {

    apiKey: "AIzaSyAkXUl4FmvW5UbD4qMpKgrWUFHD_o3AJTs",
    authDomain: "controle-yohan.firebaseapp.com",
    projectId: "controle-yohan",
    storageBucket: "controle-yohan.appspot.com",
    messagingSenderId: "820957436853",
    appId: "1:820957436853:web:90264fe2676eab8aac17fc",
    measurementId: "G-T9MBEW5LZ6"
  
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
